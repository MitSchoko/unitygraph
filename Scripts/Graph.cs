﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graph : MonoBehaviour
{

	public static Graph Instance;

	void Awake ()
	{
		Instance = this;
	}

    public class ProtectedLayoutArea
    {
        public Vector3 center;
        public float radius;
        public ProtectedLayoutArea( Vector3 center, float radius )
        {
            this.center = center;
            this.radius = radius;
        }
    }

    public List<ProtectedLayoutArea> protectedAreas = new List<ProtectedLayoutArea>();
    

    public static void ShowLayers( LayerMask mask)
    {
        foreach ( Node n in Node.All )
        {
            n.gameObject.SetActive(  (((1 << n.gameObject.layer) & mask.value) != 0) );
            
        }
        foreach ( Edge e in Edge.All)
        {
            if (e == null) continue;
            e.gameObject.SetActive(
                        ((1 << e.start.gameObject.layer) & mask.value) != 0
                      &&                      
                        ((1 << e.end.gameObject.layer) & mask.value) != 0
                );
        }

        if ( SelectedNode.current != null )
        {
            if (SelectedNode.current.gameObject.activeSelf)
            {
                SelectedNode.ReSelect();
            } else
            {
                SelectedNode.current = null;
            }
        }

    }


	[Header ("Prefabs")]
	public Node NodePrefab;
	public Edge EdgePrefab;

	[Header ("Layouting")]
	[Range (0, 100)]
	public float Speed = 1.0f;

	[Range (0, 1)]
	public float RecenteringFactor = 1.0f;

	[Range (0, 1)]
	public float CenterForce = 1.0f;
	public AnimationCurve CenterForceCurve = AnimationCurve.EaseInOut (3.0f, 1.0f, 10.0f, 0.0f);

	[Range (0, 1)]
	public float Attraction = 1.0f;
	public AnimationCurve AttractionCurve = AnimationCurve.EaseInOut (3.0f, 0.0f, 10.0f, 1.0f);
	[Range (0, 1)]
	public float Repulsion = 1.0f;
	public AnimationCurve RepulsionCurve = AnimationCurve.EaseInOut (8.0f, 1.0f, 15.0f, 0.0f);

    public static Dictionary<string, Node> ByName = new Dictionary<string, Node>();

    static Bounds _bounds;
    public static Bounds bounds
    {
        get
        {
            return _bounds;
        }
    }

    public static string GetUniqueNodeName( string name = null )
    {
        if ( string.IsNullOrEmpty( name ) )
        {
            int i = ByName.Count;
            while( ByName.ContainsKey( i.ToString() ) )
            {
                ++i;
            }
            name = i.ToString();
        } else if (ByName.ContainsKey( name ) ) {
            int i = ByName.Count;
            while (ByName.ContainsKey( name + i.ToString()))
            {
                ++i;
            }
            name +=  i.ToString();
        }
        return name;
    }

	public static Node CreateNode (Node prefab, Vector3 position, string name = null )
	{
		Node created = Instantiate<Node> (prefab == null ? Instance.NodePrefab : prefab, position, Quaternion.identity, Instance.transform);
        name = GetUniqueNodeName(name);
        created.name = name;
        ByName.Add(name, created);
        return created;
	}

    public static Node CreateNode( Vector3 position, string name = null)
    {
        return CreateNode(Instance.NodePrefab, position, name);
    }


    public static Node CreateNode(string name = null)
    {
        return CreateNode(Vector3.zero, name);
    }

    public static Node GetOrCreateNode(string name)
    {
        if (ByName.ContainsKey(name)) return ByName[name];
        else return CreateNode(name);
    }

    public static Edge Connect (Node start, Node end, Edge prefab = null)
	{
        if (start.IsConnectedTo(end))
        {
            return start.Connected[end];
        }
		Edge created = Instantiate<Edge> ( (prefab == null) ? Instance.EdgePrefab : prefab, Instance.transform);
		created.start = start;
		created.end = end;
		start.AddConnection (end, created);
    
		end.AddConnection (start, created);
		return created;
	}

    public static Edge ConnectOrStrengthen( Node start, Node end, float strength)
    {
        Edge e = null;
        if (!start.Connected.ContainsKey(end)) {
            e = Connect(start, end);
            e.Strength = strength;
        } else {
            e = start.Connected[end];
            e.Strength += strength;
        }
        return e;
    } 


	public bool Layout = false;

	void Update ()
	{
		CalculateCenter ();
		if (Layout) {
			CalculateForces (Speed * Time.deltaTime);
			//PrintForceStatistics ();
			ApplyForces ();
		}
	}

    void LateUpdate()
    {
        foreach (Edge e in Edge.All)
        {
            if (e.isActiveAndEnabled)
            {
                e.UpdatePosition();
                e.UpdateHightLight();

            }
        }
    }

    public static Vector3 Center;

	void CalculateCenter ()
	{
		Center = Vector3.zero;
		foreach (Node node in Node.All) {
            if (node == null) continue;
			Center += node.transform.position;
		}
		Center /= Node.All.Count;
	}

	Dictionary<Node, Vector2> Forces = new Dictionary<Node, Vector2> ();
    [Range(0.0f, 2.0f)]
    public float DistanceFactor = 1.0f;
    [Range(0.0f, 2.0f)]
    public float CircleFactor = 1.0f;
    [Range(0.0f, 2.0f)]
    public float CircleCenteringFactor = 1.0f;
    [Range(0.0f, 2.0f)]
    public float EdgeAvoidFactor = 1.0f;

    public void PrintForceStatistics ()
	{
		float sum = 0.0f;
		foreach (KeyValuePair< Node, Vector2 > n in Forces) {
			sum += n.Value.magnitude;
		}
		float mean = sum / Forces.Count;
		float variance = 0.0f;
		foreach (KeyValuePair< Node, Vector2 > n in Forces) {
			variance += Mathf.Pow (n.Value.magnitude - mean, 2.0f);
		}
		variance /= Forces.Count;
		Debug.Log ("sum " + sum + " mean " + mean + " var " + variance);
	}

    List<Node> ToRemove = new List<Node>();
	void ApplyForces ()
	{
        _bounds = new Bounds();
        ToRemove.Clear();
		foreach (KeyValuePair< Node, Vector2 > n in Forces) {
            if ( !Node.All.Contains(n.Key)) {
                ToRemove.Add(n.Key);
                continue;
            }
			if (n.Key.AffectedByLayouting) {
				n.Key.transform.position += (Vector3) (Speed * n.Value);
                _bounds.Encapsulate(n.Key.spriteRenderer.bounds);
			}

		}
        foreach ( Node r in ToRemove) Forces.Remove( r );
	}
#if false
    void CalculateForces (float delta)
	{
		Vector3 RecenteringForce = -RecenteringFactor * Center;
		foreach (Node node in Node.All) {
			if (! node.isActiveAndEnabled || !node.AffectedByLayouting) {
				Forces [node] = Vector3.zero;
				continue;
			}
			Vector2 force = RecenteringForce;
            foreach ( ProtectedLayoutArea l in protectedAreas )
            {
                Vector2 outVec =   node.transform.position - l.center;
                float d = outVec.magnitude;
                if ( d < l.radius)
                {
                    force += Time.deltaTime * (l.radius - d ) * outVec.normalized;
                }
            }
			float connectedFactor = Attraction / (node.Connected.Count);
			float nonConnectedFactor = Repulsion / (Node.All.Count - node.Connected.Count);
			
			float centerDist = Vector2.Distance(node.transform.position, Center );
            if (centerDist < 0.001f)
            {
                centerDist = 0.001f;
            }
            //force += CenterForce * (delta / centerDist) * CenterForceCurve.Evaluate (centerDist) * (Vector2) node.transform.position;
            force += Time.deltaTime * node.CircleForce();
			foreach (Node other in Node.All) {
				if (node == other || ! other.isActiveAndEnabled) {
					continue;
				} else if (node.IsConnectedTo (other)) {
					Vector2 dir = other.transform.position - node.transform.position;
					float length = dir.magnitude;
                    if (length == 0.0f)
                    {
                        length = 0.0001f;
                    }
					force += other.LayoutMultiplier *  connectedFactor * (delta / length) * AttractionCurve.Evaluate (length) * dir;
				} else {
					Vector2 dir = node.transform.position - other.transform.position;
					float length = dir.magnitude;
                    if (length == 0.0f)
                    {
                        length = 0.0001f;
                    }
                    force += other.LayoutMultiplier * nonConnectedFactor * (delta / length) * RepulsionCurve.Evaluate (length) * dir;
				}
			}
			if (!Forces.ContainsKey (node)) {
				Forces.Add (node, force);
			} else {
				Forces [node] = force;
			}
		}
	}
#else
    struct CirclePositioner
    {
        public float deg;
        public Node node;
        public Vector3 pos;
        public CirclePositioner( Node node, Vector3 pos )
        {
            this.node = node;
            this.pos = pos;
            this.deg = ( Mathf.Rad2Deg * Mathf.Atan2(pos.y, pos.x) + 360.0f ) % 360.0f;
        }
    }
    List<CirclePositioner> positioners = new List<CirclePositioner>();


    void CalculateForces(float delta)
    {
        Vector3 centerSum = Vector3.zero;
        int vailidCount = 0;
        float maxConnectedness = 0.0f;
        float maxLayoutFactor = 0.0f;
        foreach (Node node in Node.All)
        {

            ++vailidCount;
            centerSum += node.transform.position; 
            maxConnectedness = Mathf.Max(maxConnectedness, (float) node.Connected.Count);
            maxLayoutFactor = Mathf.Max(maxLayoutFactor, (float)node.LayoutMultiplier);
        }
        Vector3 centerPosition = Vector3.zero;
        if (vailidCount > 0)
        {
            centerPosition = (1.0f / vailidCount) * centerSum;
        }
        Vector2 centeringDirection =  - Time.deltaTime * centerPosition;
        foreach (Node node in Node.All )
        {
            if (! node.isActiveAndEnabled || ! node.AffectedByLayouting )
            {
                continue;
            }

            if (!Forces.ContainsKey(node))
                {
                    Forces.Add(node, centeringDirection);
                }
                else
                {
                    Forces[node] = centeringDirection;
                }
            
        }
        foreach (Node node in Node.All)
        {
                if ( ! node.isActiveAndEnabled)
                {
                    continue;
                }
            float selfDist = 2 * node.Size ;
       
            if (node.Connected.Count > 1)
            {
                positioners.Clear();
                foreach (Node other in node.Connected.Keys)
                {

                    if (other == null || !other.isActiveAndEnabled)
                    {
                        continue;
                    }

                    Vector3 otherDir = other.transform.position - node.transform.position;

                    float otherDist = 
                        (other.Connected.Count + 1) 
                        // * 2 
                        * other.Size;
                    float targetDist = otherDist + selfDist;
                    float currentDist = otherDir.magnitude;

                    if (currentDist < 0.01f)
                    {
                        currentDist = 0.1f;
                        float rand = Random.Range(-Mathf.PI, Mathf.PI);
                        otherDir = new Vector3( Mathf.Sin( rand ), Mathf.Cos(rand) );
                    }
                    else
                    {
                        otherDir /= currentDist;
                    }
                    float distDiff = targetDist - currentDist;
                    positioners.Add(new CirclePositioner(other, otherDir));

                    float influenceFactor = 1.0f;
                    if (other.Connected.Count > 0)
                    {
                        influenceFactor = (float)node.Connected.Count / (float)other.Connected.Count;
                    }
                    float distanceFactor =  
                    // (distDiff * distDiff) / Mathf.Max( 1.0f, targetDist) ; 
                    Mathf.Sign(distDiff) * Mathf.Exp(-(1.0f / Mathf.Abs(distDiff)));
                    float factor =  DistanceFactor * influenceFactor * distanceFactor;
                    Forces[other] += Time.deltaTime * factor * new Vector2(otherDir.x, otherDir.y);
                }
                if (positioners.Count > 0)
                {
                    positioners.Sort((a, b) => (int)Mathf.Sign(a.deg - b.deg));
                    float equidist = 360.0f / positioners.Count;
                    float minDeg = 999.0f;
                    float maxDeg = 0.0f;

                    Vector3 circleCenter = Vector3.zero;
                    for (int i = 0; i != positioners.Count; ++i)
                    {

                        CirclePositioner prev = positioners[(i == 0) ? (positioners.Count - 1) : i - 1];

                        CirclePositioner p = positioners[i];
                        circleCenter += p.node.transform.position;
                        if (p.deg < minDeg)
                        {
                            minDeg = p.deg;
                        }
                        if (p.deg > maxDeg)
                        {
                            maxDeg = p.deg;
                        }
                        CirclePositioner next = positioners[(i + 1) % positioners.Count];
                        float pd = p.deg - prev.deg;
                        if (prev.deg > p.deg)
                        {
                            pd = p.deg - (prev.deg - 360.0f);
                        }
                        float nd = next.deg - p.deg;
                        if (next.deg < p.deg)
                        {
                            nd = (next.deg + 360.0f) - p.deg;
                        }
                        float dd = (equidist - pd) - (equidist - nd);
                        float dir = -dd / equidist;
                        float influenceFactor = (float)node.Connected.Count / (float)p.node.Connected.Count;
                        float distanceFactor = Vector3.Distance(node.transform.position, p.node.transform.position);
                        float factor = CircleFactor * influenceFactor;
                        Forces[p.node] += Time.deltaTime * factor * dir * new Vector2(p.pos.y, -p.pos.x);

                    }
                    circleCenter *= (1.0f / (float) positioners.Count);
                   Forces[node] += Time.deltaTime  * CircleCenteringFactor * (Vector2)(circleCenter - node.transform.position);
                }
              
            }
            Vector3 edgeAvoid = Vector3.zero;
            foreach ( Edge e in Edge.All )
            {
                if ( e == null || e.IsInvolved( node ) )
                {
                    continue;
                }
                Vector3 closestPoint = e.ClosestPoint(node.transform.position);
                Vector3 dirFromCP =   node.transform.position - closestPoint;
                float distToCP = dirFromCP.magnitude;
                float minDist = 2.0f * node.Size;
         
               // if (distToCP < minDist)
               // {
                //    Debug.DrawLine(closestPoint, node.transform.position, Color.Lerp(Color.white, Color.green, distToCP / minDist));

                    float factor = (1.0f / ( Mathf.Max(distToCP, 0.1f) * Mathf.Max(distToCP - minDist, 0.1f) ) );
                    Vector2 avoidForce = Time.deltaTime * EdgeAvoidFactor * factor * dirFromCP;
                    Forces[node] += avoidForce;
                    Forces[e.start] -= avoidForce;
                    Forces[e.end] -= avoidForce;
               // }
                
            }

        }

    }
#endif
        }
