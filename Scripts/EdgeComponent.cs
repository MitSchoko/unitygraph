﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgeComponent : MonoBehaviour {

    Edge _edge;
    public Edge edge
    {
        get
        {
            if (_edge == null)
            {
                _edge = GetComponent<Edge>();
            }
            return _edge;
        }
    }
}
