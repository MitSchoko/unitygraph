﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectableNode : NodeComponent, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler {
    public string ToolTipWhenSelected = "Click to go to overview";
    public string ToolTipWhenDeselected = "Click to inspect";

	public void OnPointerClick( PointerEventData evtData )
    {
        if (!node.CanSelect) return;
        if (SelectedNode.current == node )
        {
            SelectedNode.current = null;
        }
        else 
        {
            SelectedNode.current = node;
        }
    }

    public void OnPointerEnter( PointerEventData evtData )
    {
        if (!node.CanHoover) return;    
        SelectedNode.Hoover(node);
    }
   
    public void OnPointerExit( PointerEventData evtData )
    {
        if (!node.CanHoover) return;
        SelectedNode.Hoover(null);
    }

}
