﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Edge : MonoBehaviour
{
	public static HashSet<Edge> All = new HashSet<Edge> ();

	public Node start;
	public Node end;

    public bool IsInvolved( Node other )
    {
        return other == start || other == end;
    }

	public float Strength = 1.0f;

	public float distance = 2.0f;
	public float dampingRatio = 0.2f;
    public SpriteRenderer FillA;
    public SpriteRenderer FillB;


    float _fillValue = 0.0f;
    public float fillValue
    {
        get
        {
            return _fillValue;
        }
        set
        {
            _fillValue = Mathf.Clamp( value, - 1.0f, 1.0f );
            UpdateFill();
        }
    }


    void UpdateFill( )
    {
        if (FillA != null && FillB != null)
        {
            if (_fillValue == 0)
            {
                spriteRenderer.enabled = true;
                FillA.gameObject.SetActive(false);
                FillB.gameObject.SetActive(false);
            }
            else
            {
                spriteRenderer.enabled = false;
                FillA.gameObject.SetActive(true);
                FillB.gameObject.SetActive(true);
                FillA.transform.localScale = new Vector3(Mathf.Abs(fillValue), 1, 1);
                FillB.transform.localScale = new Vector3(1.0f - Mathf.Abs(fillValue), 1, 1);
                if (fillValue > 0 )
                {
                    FillA.transform.localPosition = new Vector3( - 0.5f * ( 1 - fillValue), 0.0f, - 0.0f);
                    FillB.transform.localPosition = new Vector3(1.0f  - (1.0f - ( 0.5f  *  fillValue)), 0.0f, -0.0f);
                }
                else
                {
                    FillA.transform.localPosition = new Vector3(  0.5f * (1 -  Mathf.Abs(fillValue)), 0.0f, - 0.0f);
                    FillB.transform.localPosition = new Vector3(   - ( 0.5f * Mathf.Abs( fillValue )), 0.0f, - 0.0f);
                }
            }
        }
    }

	[SerializeField]
	SpriteRenderer _spriteRenderer;

	public SpriteRenderer spriteRenderer {
		get {
			if (_spriteRenderer == null) {
				_spriteRenderer = GetComponent<SpriteRenderer> ();
			}
			return _spriteRenderer;
		}
	}

    public Vector3 directionVector
    {
        get
        {
            return end.transform.position - start.transform.position;
        }
    }

    public Vector3 ClosestPoint( Vector3 point)
    {
        if (start == null || end == null)
        {
            return Vector3.zero;
        }
        Vector3 startToPoint = point - start.transform.position ;
        Vector3 egdeVector = end.transform.position - start.transform.position;
        float sqrMag = egdeVector.sqrMagnitude;
        float mag = Mathf.Sqrt(sqrMag);
        float t = Vector3.Dot( startToPoint, egdeVector ) / sqrMag;
        if ( t <= 0.0f )
        {
            return start.transform.position;
        } else if ( t >= 1.0f )
        {
            return end.transform.position;
        }
        return  start.transform.position + (t * egdeVector);
    }
 
    public Color color
    {
        get
        {
            return spriteRenderer.color;
        }
        set
        {
            spriteRenderer.color = value;
        }
    }


	void Awake ()
	{
		All.Add (this);
	}




	public void UpdatePosition ()
	{
        Vector3 dir = end.transform.position - start.transform.position;
        float dirLen = dir.magnitude;
        if (dirLen < 0.01f) return;
        dir /= dirLen;
        float size = 0.8f * Strength + 0.2f;

        float startCircleSize = Mathf.Clamp01(Mathf.Cos(size / start.Size)) * start.Size;
        float endCircleSize   = Mathf.Clamp01(Mathf.Cos(size / end.Size)) * end.Size;

        float dirCutLen = dirLen - 0.5f * (startCircleSize + endCircleSize);
        transform.localPosition = start.transform.position +  0.5f * startCircleSize * dir + 0.5f * dirCutLen * dir;
        transform.localScale = new Vector3 (dirCutLen, size , 1.0f);
		transform.localRotation = Quaternion.FromToRotation (Vector3.right, dir);
	}


    public bool Highlighted
    {
        get
        {
            if (start == null || end == null) return false;
            return (start.Highlighted && end.Highlighted); 
        }
    }

     public void UpdateHightLight( )
    {
        int o = Highlighted ? 5 : 1;
        spriteRenderer.sortingOrder = o;
        FillA.sortingOrder = o;
        FillB.sortingOrder = o;
    }



    public void Delete( )
    {

        if (this.start != null) this.start.DisconnectNode(this.end);
        if (this.end != null) this.end.DisconnectNode(this.start);
        IDeleteEffect effect = GetComponent<IDeleteEffect>();
        if (effect != null)
        {
            effect.RunEffect(() =>
            {
                Destroy(gameObject);
            });
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        Edge.All.Remove(this);
        if (this.start != null) this.start.DisconnectNode(this.end);
        if (this.end != null) this.end.DisconnectNode(this.start);
    }
}
