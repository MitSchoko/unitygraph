﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class GraphRaycaster : BaseRaycaster
{
    public static bool AllowInteractions = true;

	[SerializeField]
	Camera _camera;

	public override Camera eventCamera {
		get {
			if (_camera == null) {
				_camera = GetComponent<Camera> ();
			}
			return _camera;
		}
	}


    public override void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
    {
        if ( ! AllowInteractions )
        {
            return;
        }
        Vector3 mousePos = eventCamera.ScreenToWorldPoint(eventData.position);
     //   Debug.Log(mousePos);
        foreach (Node node in Node.All)
        {
            if (Vector2.Distance(mousePos, node.transform.position) <= node.Size)
            {
                RaycastResult rr = new RaycastResult();
                rr.screenPosition = eventData.position;
                rr.worldPosition = mousePos;
                rr.module = this;
                rr.gameObject = node.gameObject;
                resultAppendList.Add(rr);
            }
        }
    }

}
