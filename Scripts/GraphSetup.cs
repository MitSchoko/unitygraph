﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphSetup : MonoBehaviour
{
    public float ClusterSize = 3.0f;
    public float Size = 100.0f;
    public int GroupedPopulation = 80;
	public int MinGroupSize = 4;
	public int MaxGroupSize = 14;
	public float MinGroupConnectivity = 0.7f;
	public float MaxGroupConnectivity = 0.99f;
	public int LonerPopulation = 20;
	public float LonerConnectivity = 0.1f;
	public float GlobalConnectivity = 0.05f;

    public int AssureMinimumConnections = 3;

    private void Start()
    {
        var graph = GetComponent<Graph>();
        CreateCircle(graph.NodePrefab, Vector3.zero, 8.0f, 20, 5, 6, graph.EdgePrefab);
    }

    public static List<Node> CreateCircle(Node prefab, Vector3 center, float size, int nodeCount, int minConnections, int maxConnections, Edge edge = null)
	{
		List<Node> nodes = new List<Node> ();

		float step = (2.0f * Mathf.PI) / (nodeCount);
		float t = 0;
		for (int i = 0; i != nodeCount; ++i) {
			Vector3 pos = center + size * new Vector3 (Mathf.Cos (t), Mathf.Sin (t));
			t += step;
			Node created = Graph.CreateNode (prefab, pos);
			nodes.Add (created);
		}
        if (maxConnections < 0 )
        {
            maxConnections = Mathf.Max( nodeCount + maxConnections, 0);
        }
        maxConnections = Mathf.Clamp( maxConnections, 0, nodeCount - 1 );
        InternalyConnect(nodes, minConnections, maxConnections, edge);

		return nodes;
	}

	public static List<Node> CreateRandom ( Node prefab, Vector3 center, float size, int nodeCount, int minConnections, int maxConnections )
	{
		List<Node> nodes = new List<Node> ();

		for (int i = 0; i != nodeCount; ++i) {
			Vector3 pos = center + size * ((Vector3)Random.insideUnitCircle);
			Node created = Graph.CreateNode (prefab, pos);
			nodes.Add (created);
		}
        InternalyConnect(nodes, minConnections, maxConnections);
		return nodes;
	}

    public static void InternalyConnect( List<Node> nodes, int minConnections, int maxConnections, Edge prefab = null)
    {
        maxConnections = Mathf.Min(nodes.Count, maxConnections);
        Dictionary<Node, HashSet<Node>> connections = new Dictionary<Node, HashSet<Node>>();
        Dictionary<Node, int> intendedConnections = new Dictionary<Node, int>();
        foreach (Node n in nodes)
        {
            connections.Add(n, new HashSet<Node>());
            intendedConnections.Add(n, Random.Range(minConnections, maxConnections + 1));
        }

        foreach (Node n in nodes)
        {
            if (intendedConnections[n] <= 0) continue;
            List<Node> possible = new List<Node>(nodes);
            possible.RemoveAll(other => other == n || intendedConnections[other] == 0);
            while (intendedConnections[n] > 0)
            {
                if (possible.Count == 0) break;
                int chooice = Random.Range(0, possible.Count);
                Node other = possible[chooice];
                possible.RemoveAt(chooice);
                Graph.Connect(n, other, prefab);
                intendedConnections[other]--;
                intendedConnections[n]--;
            }
        }
    }

    public static void InternalyConnectOrStrengthen(List<Node> nodes, int minConnections, int maxConnections, float minStrengthen, float maxStrengthen)
    {
        if (nodes.Count == 0) return;
        while (maxConnections < 0) maxConnections += nodes.Count;
        maxConnections = Mathf.Min(nodes.Count -1 , maxConnections);
        Dictionary<Node, HashSet<Node>> connections = new Dictionary<Node, HashSet<Node>>();
        Dictionary<Node, int> intendedConnections = new Dictionary<Node, int>();
        foreach (Node n in nodes)
        {
            connections.Add(n, new HashSet<Node>());
            intendedConnections.Add(n, Random.Range(minConnections, maxConnections + 1));
        }

        foreach (Node n in nodes)
        {
            if (intendedConnections[n] <= 0) continue;
            List<Node> possible = new List<Node>(nodes);
            possible.RemoveAll(other => other == n || intendedConnections[other] == 0);
            while (intendedConnections[n] > 0)
            {
                if (possible.Count == 0) break;
                int chooice = Random.Range(0, possible.Count);
                Node other = possible[chooice];
                possible.RemoveAt(chooice);
                Graph.ConnectOrStrengthen(n, other, Random.Range(minStrengthen, maxStrengthen));
                intendedConnections[other]--;
                intendedConnections[n]--;
            }
        }
    }


    public static void ConnectGroups (IEnumerable<Node> source, IEnumerable<Node> target, int minConnections, int MaxConnections, Edge prefab = null) 
	{
    
        List<Node> targetList = new List<Node>(target);
        targetList.Randomize();
        int targetListIndex = 0;
		foreach ( Node node in source )
        {

            int expectedConnections = Random.Range(minConnections, Mathf.Min(targetList.Count, MaxConnections + 1));
            for (int i = 0; i != expectedConnections; ++i)
            {
                Node other = targetList[targetListIndex];
                targetListIndex = ++targetListIndex % targetList.Count;
                Graph.Connect(node, other, prefab);
            }
        }
	}

    public static void ConnectClusters( List<List<Node>> clusters, int minConnections, int maxConnections ) {
        for (int i = 0; i != clusters.Count; ++i ) {
            List<Node> others = new List<Node>();
            for (int j = 0; j != clusters.Count; ++j )
            {
                if (i == j) continue;
                others.AddRange(clusters[j]);
            }
            ConnectGroups(clusters[i], others, minConnections, maxConnections);
        }
    }

}
