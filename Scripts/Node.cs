﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour { 
	public static HashSet<Node> All = new HashSet<Node> ();
	public static Node Hoovered;

    public bool CanSelect = true;
    public bool CanHoover = true;

	[SerializeField]
	SpriteRenderer _spriteRenderer;

	public SpriteRenderer spriteRenderer {
		get {
			if (_spriteRenderer == null) {
				_spriteRenderer = GetComponent<SpriteRenderer> ();
			}
			return _spriteRenderer;
		}
	}

    public SpriteRenderer Symbol;

    public bool PlaceOnCircle = false;
    public Vector2 CircleCenter;
    public float CircleDistance;
    public float CircleForceFactor = 1.0f;

    public Vector2 CircleForce( )
    {
        if (!PlaceOnCircle) return Vector2.zero;
        Vector2 CircleDirection = (Vector2)transform.position - CircleCenter;
        float distanceFromCenter = CircleDirection.magnitude;
        CircleDirection /= distanceFromCenter;
        Vector2 TargetPosition = CircleDistance * CircleDirection;
        return CircleForceFactor * ( TargetPosition - (Vector2)transform.position );
    }


    public event System.Action<Node, Edge> AddedConnection;
    public event System.Action<Node, Edge> RemovedConnection;

    public int degree
    {
        get
        {
            return Connected.Count;

        }

    }

    public void AddConnection( Node node, Edge edge)
    {
        Connected.Add(node, edge);
        if (AddedConnection != null)
        {
            AddedConnection(node, edge);
        }
    }

    public void DisconnectNode(Node other)
    {
        if (Connected.ContainsKey(other))
        {
            Edge edge = Connected[other];
            Connected.Remove(other);
            if (RemovedConnection != null)
            {
                RemovedConnection(other, edge);
            }
        }
    }

    public void GetComponentsInConnected<T>( List<T> appendResults )
    {
        List<T> nodeResult = new List<T>();
        foreach ( Node n in Connected.Keys )
        {
            if (n == null) continue;
            n.GetComponents(nodeResult);
            appendResults.AddRange(nodeResult);
        }
    }

    public IEnumerable GetComponentsInConnected<T>()
    {
        foreach (Node n in Connected.Keys)
        {
            if (n == null) continue;
            foreach ( T c in  n.GetComponents<T>() )
            {
                yield return c;
            }
           
        }
    }


    public float DefaultSize = 0.5f;
    public float HighlightSize = 0.8f;
	public float LayoutMultiplier = 1.0f;
    
	public float Size {
		get {
			return transform.localScale.x;
		}
		set {
			transform.localScale = new Vector3 (value, value, 1.0f);
		}
	}

	bool _Highlighted;

    

	public bool Highlighted {
		get {
			return _Highlighted;
		}
		set {
		//	spriteRenderer.color = Color.Lerp (DefaultColor, HighlightColor, value);
			_Highlighted = value;
            spriteRenderer.sortingOrder = value ? 6 : 2;
            if (Symbol != null) Symbol.sortingOrder = value ? 7 : 3;
			//LayoutMultiplier = value + 1.0f;
			//Size = Mathf.Lerp( DefaultSize, HighlightSize, value );
    
        }
	}


	public Dictionary<Node, Edge> Connected = new Dictionary<Node, Edge> ();

    public IEnumerable<Edge> edges
    {
        get
        {
            foreach (Edge e in Connected.Values) if (e != null) yield return e;
        }
    }  

	public bool AffectedByLayouting = true;

    public GameObject LabelPivot;

    public void EnableLabel()
    {
        if (LabelPivot != null && !LabelPivot.activeSelf)
        {
            LabelPivot.SetActive(true);
        }
    }

    public void DisableLabel()
    {
        if (LabelPivot != null && LabelPivot.activeSelf)
        {
            LabelPivot.SetActive(false);
        }
    }

    public bool showLabel
    {
        get
        {
            if (LabelPivot == null)
            {
                return false;
            } else
            {
                return LabelPivot.activeSelf;
            }

        }
        set
        {
            if (value)
            {
                EnableLabel();
            } else
            {
                DisableLabel();
            }
        }
    }

    public bool IsConnectedTo (Node node)
	{
		return Connected.ContainsKey (node);
	}

   

	void Awake ()
	{
		All.Add (this);
        
	}

    private void Start()
    {
        Size = DefaultSize;
    }

    public Color color
    {
        get
        {
            return spriteRenderer.color;
        }
        set
        {
            spriteRenderer.color = value;
        }
    }

    public void Delete( )
    {
        All.Remove(this);
        List<Edge> connected = new List<Edge>(Connected.Values);
        foreach (Edge edge in connected)
        {
            if (edge != null)
            {
                edge.Delete();
            }
        }
        Connected.Clear();
        IDeleteEffect effect = GetComponent<IDeleteEffect>();
        if (effect != null)
        {
            effect.RunEffect(() =>
            {
                Destroy(gameObject);
            });
        } else {
            Destroy(gameObject);
        }
    }

    public void CenterBetweenConnected()
    {
        if (Connected.Count == 0) return;
        Vector3 Center = Vector3.zero;
        foreach ( Node n in Connected.Keys )
        {
            Center += n.transform.position;
        }
        Center *= 1.0f / Connected.Count;
        transform.position = Center;
    }


    void OnDestroy( )
    {
        All.Remove(this);
        List<Edge> connected = new List<Edge>(Connected.Values);

        foreach ( Edge edge in connected)
        {
            if (edge != null)
            {
                Destroy(edge.gameObject);
            }
        }
    }
}
