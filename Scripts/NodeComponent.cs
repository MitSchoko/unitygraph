﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class  NodeComponent : MonoBehaviour {
    Node _node;
    public Node node
    {
        get
        {
            if (_node == null)
            {
                _node = GetComponent<Node>();
            }
            return _node;
        }
    }


}
