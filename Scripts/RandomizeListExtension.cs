﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RandomValue
{

	public static int LowerThan (int lowerThan)
	{
		return Random.Range (0, lowerThan);
	}

	public static void Randomize<T> (this IList<T> list)
	{
		for (int n = list.Count - 1; n > 1; --n) {
			int k = LowerThan (n + 1);
			T tmp = list [k];
			list [k] = list [n];
			list [n] = tmp;
		}
	}
}
