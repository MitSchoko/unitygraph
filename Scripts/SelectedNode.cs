﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedNode : MonoBehaviour {

    public static event System.Action<Node> Changed;
    static Node _current;
    public static Node current
    {
        get
        {
            return _current;
        }
        set
        {
            Node previous = _current;
            if (_current != null && value != _current)
            {
                Instance.DeSelect();
            }
            _current = value;
            if (_current != null)
            {
                Instance.Select();
            }
            if (Changed != null) Changed(_current);
            if (! inHistoryOperation && _current != previous && _current != null)
            {
                    if (currentHistoryPosition < History.Count)
                    {
                        History.RemoveRange(currentHistoryPosition + 1, History.Count - currentHistoryPosition);
                    }
                    History.Add(_current);
                    ++currentHistoryPosition;
            }
        }
    }

    public static void ReSelect( )
    {
        if (Instance != null && current != null) Instance.Select();
    }

    public static SelectedNode Instance;
    public float TransistionSpeed = 50.0f;
    void Awake( )
    {
        Instance = this;
    }

    public static void Hoover( Node node )
    {
        Instance.HooverNode(node);
    }

    public static void HooverSelection( IEnumerable<Node> nodes)
    {
        Instance.HooverNodes(nodes);
    }


    public void HooverNode( Node node )
    {
        if (current != null) return;
        selectedNeighbours.ForEach(n =>
        {
            n.Highlighted = false;
            n.showLabel = false;
        });
        selectedNeighbours.Clear();
        if (node != null)
        {
            BackroundFader.FadeIn(0.2f);
            selectedNeighbours.Add(node);
            selectedNeighbours.AddRange(node.Connected.Keys);
            selectedNeighbours.ForEach(n => {
                n.Highlighted = true;
                n.showLabel = true;

            });
        } else
        {
            BackroundFader.FadeOut(0.2f);
        }
    }


    #region History
    static List<Node> History = new List<Node>();
    static int currentHistoryPosition = 0;
    static bool inHistoryOperation = false;




    public static bool CanGoBackInHistory
    {
        get
        {
            return currentHistoryPosition > 0;
        }
    }

    public static void GoBackInHistory( )
    {
        if (!CanGoBackInHistory) return;
        inHistoryOperation = true;
        current = History[--currentHistoryPosition];
        inHistoryOperation = false;
    }

    public static bool CanGoForwardInHistory
    {
        get
        {
            return currentHistoryPosition < History.Count;
        }
    }

    public static void GoForwardInHistory()
    {
        if (!CanGoForwardInHistory) return;
        inHistoryOperation = true;
        current = History[++currentHistoryPosition];
        inHistoryOperation = false;
    }


    #endregion

    public void HooverNodes( IEnumerable<Node> nodes)
    {
        if (current != null) return;
        selectedNeighbours.ForEach(n =>
        {
            n.Highlighted = false;
            n.showLabel = false;
        });
        selectedNeighbours.Clear();
        selectedNeighbours.AddRange(nodes);
        if (selectedNeighbours.Count > 0) 
        {
            BackroundFader.FadeIn(0.2f);

            selectedNeighbours.ForEach(n => {
                n.Highlighted = true;
                n.showLabel = true;

            });
        }
        else
        {
            BackroundFader.FadeOut(0.2f);
        }
    }


    Vector3 ResetPosition;
    float ResetSize;
    public void Select()
    {
        if (currentSelectCoroutine != null)
        {
            StopCoroutine(currentSelectCoroutine);
        } else
        {
            ResetPosition = CameraControl.position;
            ResetSize = CameraControl.size;
        }
        currentSelectCoroutine = SelectCoroutine();
        if (currentProtectedArea != null)
        {
            Graph.Instance.protectedAreas.Remove(currentProtectedArea);
            currentProtectedArea = null;
        }
        BackroundFader.FadeIn(transitionTime);
        StartCoroutine(currentSelectCoroutine);
        current.BroadcastMessage("OnBecameSelected", SendMessageOptions.DontRequireReceiver);
    }

    IEnumerator currentSelectCoroutine;
    Graph.ProtectedLayoutArea currentProtectedArea;

    List<Node> selectedNeighbours = new List<Node>();
    public float transitionTime = 0.5f;
    IEnumerator SelectCoroutine()
    {
        current.AffectedByLayouting = false;
        current.Highlighted = true;
        current.EnableLabel();
        selectedNeighbours.Clear();
        selectedNeighbours.AddRange(current.Connected.Keys);
        selectedNeighbours.RemoveAll(n => !n.isActiveAndEnabled);
        selectedNeighbours.Sort(
            (a, b) =>
            {
                return (int)Mathf.Sign(
                Vector3.Distance(a.transform.position, current.transform.position)
              - Vector3.Distance(b.transform.position, current.transform.position)
              );
            }
        );
        int nodeCount = selectedNeighbours.Count;
        List<Vector3> positions = new List<Vector3>();
        float td = (2.0f * Mathf.PI) / nodeCount;
        float d = Mathf.Max((5.0f * nodeCount) / (2.0f * Mathf.PI), 5.0f);
        currentProtectedArea = new Graph.ProtectedLayoutArea(current.transform.position, 1.5f * d);
        Graph.Instance.protectedAreas.Add(currentProtectedArea);
        for (int i = 0; i != nodeCount; ++i)
        {
            positions.Add(current.transform.position + new Vector3(d * Mathf.Cos(i * td), d * Mathf.Sin(i * td)));
        }


        foreach (Node n in Node.All)
        {
            n.DisableLabel();
            n.Highlighted = false;
        }
        current.Highlighted = true;
        current.EnableLabel();
        Dictionary<Node, Vector3> targetPositions = new Dictionary<Node, Vector3>();
        foreach (Node n in selectedNeighbours)
        {
            int minIndex = 0;
            float minDist = float.MaxValue;
            for (int i = 0; i != positions.Count; ++i)
            {
                float dist = Vector3.Distance(n.transform.position, positions[i]);
                if (dist <= minDist)
                {
                    minDist = dist;
                    minIndex = i;
                }
            }
            n.AffectedByLayouting = false;
            n.Highlighted = true;
            targetPositions.Add(n, positions[minIndex]);
            positions.RemoveAt(minIndex);
        }

        float passedTime = 0.0f;
        Vector3 camStartPos = CameraControl.position;
        Vector3 camTargetPos = current.transform.position;
        camTargetPos.z = CameraControl.position.z;
        float camStartSize = CameraControl.size;
        float camTargetSize = d * 1.5f;
        Dictionary<Node, Vector3> InitalPos = new Dictionary<Node, Vector3>();
        Dictionary<Node, float> InitalSize = new Dictionary<Node, float>();
        float currentInitalSize = current.Size;
        foreach (KeyValuePair<Node, Vector3> p in targetPositions)
        {
            InitalPos.Add(p.Key, p.Key.transform.position);
            InitalSize.Add(p.Key, p.Key.Size);
        }

        float targetSize = 0.25f * d;
        while (passedTime < transitionTime) 
        {
            passedTime += Time.deltaTime;

            float t = passedTime / transitionTime;
            CameraControl.position = Vector3.Lerp(camStartPos, camTargetPos, t);
            CameraControl.size = Mathf.Lerp(camStartSize, camTargetSize, t);
            current.Size = Mathf.Lerp(currentInitalSize, targetSize , t);
            foreach (KeyValuePair<Node, Vector3> p in targetPositions)
            {
                p.Key.transform.position = Vector3.Lerp(InitalPos[p.Key], p.Value, t);
                p.Key.Size = Mathf.Lerp(InitalSize[p.Key], targetSize, t);
            }

            yield return null;

        }
        foreach (KeyValuePair<Node, Vector3> p in targetPositions)
        {
            p.Key.transform.position = p.Value;
            p.Key.Size = targetSize;
            p.Key.EnableLabel();
        }
        current.EnableLabel();
        currentSelectCoroutine = null;
    }

    

    public void DeSelect()
    {
        if (currentSelectCoroutine != null) StopCoroutine(currentSelectCoroutine);
        if (currentProtectedArea != null)
        {
            Graph.Instance.protectedAreas.Remove(currentProtectedArea);
            currentProtectedArea = null;
        }
        BackroundFader.FadeOut( 1.0f );
        current.AffectedByLayouting = true;
        current.Highlighted = false;
        current.Size = current.DefaultSize;
        foreach (Node n in selectedNeighbours)
        {
            if (n == null || ! Node.All.Contains(n)) continue;
            n.Highlighted = false;
            n.Size = n.DefaultSize;
            n.AffectedByLayouting = true;
        }
        selectedNeighbours.Clear();
        foreach ( Node n in Node.All )
        {
            n.DisableLabel();
        }
        currentSelectCoroutine = DeselectCoroutine();
        StartCoroutine(currentSelectCoroutine);
        current.BroadcastMessage("OnBecameDeSelected", SendMessageOptions.DontRequireReceiver);

    }

    IEnumerator DeselectCoroutine( )
    {
        Vector3 startPosition = CameraControl.position;
        float startSize = CameraControl.size;
        float t = 0.0f;
        while ( t < transitionTime )
        {
            t += Time.deltaTime;
            CameraControl.position = Vector3.Lerp(startPosition, ResetPosition, t / transitionTime);
            CameraControl.size = Mathf.Lerp(startSize, ResetSize, t / transitionTime);
            yield return null;
        }
        CameraControl.position = ResetPosition;
        currentSelectCoroutine = null;
    }
}
