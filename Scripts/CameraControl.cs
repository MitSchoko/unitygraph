﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public static CameraControl Instance;
    void Awake()
    {
        Instance = this;
    }
    public float MaxSizeFactor = 1.1f;
    public float MaxSize {
        get
        {
    
                return  MaxSizeFactor * Mathf.Max(
                    Mathf.Abs(Graph.bounds.min.x),
                    Mathf.Abs(Graph.bounds.min.y),
                    Mathf.Abs(Graph.bounds.max.x),
                    Mathf.Abs(Graph.bounds.max.y),
                    MinSize
                );
            
        }
           
    }
	public float MinSize = 5.0f;
	public float ScrollSpeed = 1.0f;
	public float ScrollPositionSpeed = 1.0f;

	[SerializeField]
	Camera _camera;

    Vector3 _position;
        

    public static Vector3 position
    {
        get
        {
            if (Instance == null)
            {
                return Vector3.zero;
            }
            else
            {
                return Instance._position;
            }
        }
        set
        {
            if (Instance != null)
            {
                Instance._position = value;
            }
        }
    }
    
    public static float size
    {
        get
        {
            if (Instance == null)
            {
                return 0.0f;
            }
            else
            {
                return camera.orthographicSize;
            }
        }
        set
        {
            if (Instance != null)
            {
                camera.orthographicSize = value;
            }
        }
    }
    
    public static float normalizedSize
    {
        get
        {
            return (size - Instance.MinSize) / (Instance.MaxSize - Instance.MinSize);
        }
        set
        {
            size = value * (Instance.MaxSize - Instance.MinSize) + Instance.MinSize;
        }
    }

	public static Camera camera {
		get {
			if (Instance._camera == null) {
				Instance._camera = Instance.GetComponent<Camera> ();
			}
			return Instance._camera;
		}
	}

    // Update is called once per frame
    bool hasMouseInit = true;
    Vector3 lastMousePos;

    public static bool AllowInteractions = true;
    void Update()
    {
        if (!AllowInteractions) return;
        if (SelectedNode.current != null) return;
        if (Input.GetMouseButton(0))
        {
            if (!hasMouseInit)
            {
                lastMousePos = Input.mousePosition;
                hasMouseInit = true;
            } else
            {
                Vector3 mouseDelta = Input.mousePosition - lastMousePos;
                position -= new Vector3(
                    (mouseDelta.x / Screen.width) * camera.aspect * 2.0f * size,
                    (mouseDelta.y / Screen.height) * 2.0f * size
                    );


                lastMousePos = Input.mousePosition;
            }
        } else
        {
            hasMouseInit = false;
        }

        if (Input.mouseScrollDelta.y != 0.0f) {
            Vector3 mousePos = Vector3.zero;
            Vector3 pointerOffset = Vector3.zero;

       //     if (camera.orthographicSize <= MinSize || camera.orthographicSize >= MaxSize) return;
            Vector3 mouseWorldPos = camera.ScreenToWorldPoint(Input.mousePosition);

            pointerOffset = mouseWorldPos - position;
            pointerOffset /= size;

            float newSize = Mathf.Clamp(
                size + ScrollSpeed * Time.deltaTime * Input.mouseScrollDelta.y,
                MinSize,
                MaxSize
            );
            size = newSize;
            mousePos = mouseWorldPos - (camera.orthographicSize * pointerOffset);
            mousePos.z = -20.0f;
            position = mousePos;
        }
    }
    void LateUpdate()
    {
        Vector3 pos = _position;
        if (SelectedNode.current == null) pos = Graph.bounds.ClosestPoint(_position);
        pos.z = -10.0f;
        transform.position = pos ;
    }
}
