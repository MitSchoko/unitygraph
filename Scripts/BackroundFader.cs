﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackroundFader : MonoBehaviour {
    public static BackroundFader Instance;

    [SerializeField]
    SpriteRenderer _renderer;
    public SpriteRenderer renderer
    {
        get
        {
            if (_renderer == null)
            {
                _renderer = GetComponent<SpriteRenderer>();
            }
            return _renderer;
        }
    }

    private void Awake()
    {
        Instance = this;
    }
    public Color FadeOutColor;
    public Color FadeInColor;
    float currentFade = 0.0f;

    IEnumerator currentFadeCoroutine;

    public static void FadeIn( float time )
    {

        if (Instance.currentFadeCoroutine != null)
        {
            Instance.StopCoroutine(Instance.currentFadeCoroutine);
        }
        Instance.currentFadeCoroutine = Instance.FadeInCoroutine(time);
        Instance.StartCoroutine(Instance.currentFadeCoroutine);
    }

    IEnumerator FadeInCoroutine( float time )
    {
        renderer.sortingOrder = 4;
        while (currentFade < 1.0f)
        {
            currentFade += Time.deltaTime / time;
            renderer.color = Color.Lerp(FadeOutColor, FadeInColor, currentFade);
            yield return null;
        }
        currentFade = 1.0f;
        currentFadeCoroutine = null;
    }

    public static void FadeOut(float time)
    {
        if ( Instance.currentFadeCoroutine != null)
        {
            Instance.StopCoroutine(Instance.currentFadeCoroutine);
        }
        Instance.currentFadeCoroutine = Instance.FadeOutCoroutine(time);
        Instance.StartCoroutine(Instance.currentFadeCoroutine);
    }

    IEnumerator FadeOutCoroutine(float time)
    {
        while (currentFade > 0.0f)
        {
            currentFade -= Time.deltaTime / time;
            renderer.color = Color.Lerp(FadeOutColor, FadeInColor, currentFade);
            yield return null;
        }
        renderer.sortingOrder = 0;
        currentFade = 0.0f;
        currentFadeCoroutine = null;
    }
}
