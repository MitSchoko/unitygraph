﻿using System;

internal interface IDeleteEffect
{
    void RunEffect(Action cb);
}